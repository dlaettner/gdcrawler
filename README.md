# Glassdoor Crawler

This batch is simply read company reviews and aggregates it in an Excel spreadsheet.

### libraries dependency
* beautifulsoap4
* pandas
* sqlalchemy
* xlsxwriter
* pyyaml

#### available companies
* wix
* fivver
* f5 networks

#### adding a company
_option 1_: add it to `companies.yaml` file:
```yaml
<company_name> : <company_glassdoor_id>
```
for example:
```yaml
wix: Wix-Reviews-E391615
```
> the glassdoor id is taken from the reviews address 

_option 2_: run the script with a none existing company and
it will be added to the file:
```bash
$> python main.py -c ebay
Company ebay not exists
Name: ebay
Path to reviews (exp. Wix-Reviews-E391615, Fiverr-Inc-Reviews-E750333):
eBay-Reviews-E7853
```

### run the batch
to get the list of the supported companies run : 
```bash
python main.py 
```

```
No company was selected
```

The run will create an excel file from this format `<company_name>_reviews.xlsx` 

####example
```bash
python main.py -c wix
```

####options
```bash
  -h, --help            show this help message and exit
  -o FILE, --output-file=FILE
                        file output name
  -m MODE, --mode=MODE  type of output (database or report)
  -c COMPANY, --companyname=COMPANY
                        company name to extract data
  -x SAMPLE, --sample=SAMPLE
                        run a sample of records (for test)
  -v                    verbose
```