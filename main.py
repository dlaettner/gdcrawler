import pprint
import sys
import os
from optparse import OptionParser
import yaml

import pandas as pd
import requests
from bs4 import BeautifulSoup as bs
from pandas import DataFrame
# from sqlalchemy import create_engine


def print_error(msg):
    print("\033[0;31;48m" + msg + "\033[0;37;48m")


def print_info(msg):
    print("\033[1;32;40m" + msg + "\033[0;37;48m")

parser = OptionParser()
parser.add_option("-o", "--output-file", dest="output_filename",
                  help="file output name", metavar="FILE")
parser.add_option("-m", "--mode", dest="mode", default="database",
                    help="type of output (database or report)")
parser.add_option("-c", "--companyname", dest="company", help="company name to extract data")
parser.add_option("-x", "--sample", dest="sample", help="run a sample of records (for test)")
parser.add_option("-v", action="store_true", dest="verbose", help="verbose")

(options, args) = parser.parse_args()

if options.company is None:
    print_error("No company was selected")
    exit(1)

if options.mode is not None:
    if options.mode not in ['database', 'report']:
        print_error(options.mode + " is not a valid type (database or report)")
        exit(2)

limit = 0
if options.sample is not None:
    limit = int(options.sample)

output_filename = options.company + '_reviews.xlsx'
if options.output_filename is not None:
    output_filename = options.output_filename

filename = "companies.yaml"
path = os.path.realpath(".")
full_path = path + "/" + filename

def get_company_cfg():
    exists = os.path.isfile(full_path)
    cmps = {}
    if exists:
        fh = open(full_path, 'r')
        cmps = yaml.load(fh, Loader=yaml.FullLoader)
        fh.close()
    else:
        print_error("No companies file was found,creating a new one:")
        add_company_to_file(cmps)
    return cmps


def add_company_to_file(cmps):
    print("Name: " + options.company)
    company = options.company
    print("Path to reviews (exp. Wix-Reviews-E391615, Fiverr-Inc-Reviews-E750333):")
    rvw_path = input()
    if len(rvw_path) > 0:
        cmps[company] = rvw_path
        with open(full_path, 'w') as yaml_file:
            yaml_file.write(yaml.dump(cmps, default_flow_style=False))
    else:
        print_error("Path is invalid, exiting")
        exit(2)


companies = get_company_cfg()
if companies.get(options.company) is None:
    print_error("Company " + options.company + " not exists")
    add_company_to_file(companies)

base_url = "https://www.glassdoor.com/Reviews/"
url = base_url + companies.get(options.company) + "_P1.htm"
pp = pprint.PrettyPrinter(indent=4)

reviews_data = {}
responses = {}
recommends = {}


def get_num_of_reviews(soup):
    container = soup.find(name='div',
            attrs={"class": "eiReviews__EIReviewsPageContainerStyles__EIReviewsPageContainer"})
    return container.find(name="strong").text


def get_employer_id(employer_url):
    return employer_url.split("-")[-1][1:]


def get_reviews():
    global url
    total_reviews = 0
    num_of_rvs = None
    while url is not None:
        headers = {
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en",
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) "
                          "AppleWebKit/537.36 (KHTML, like Gecko) "
                          "Chrome/64.0.3282.186 Safari/537.36"
        }
        resp = requests.get(url, headers=headers)
        soup = bs(resp.text, 'html.parser')
        if num_of_rvs is None:
            num_of_rvs = get_num_of_reviews(soup)
            if 0 < limit > int(num_of_rvs):
                print_info("sample was set to " + options.sample +
                           " but there are " + num_of_rvs + " reviews ... ignoring")
        reviews = soup.find_all(name='li', attrs={"class": "empReview"})
        total_reviews += len(reviews)
        url = get_next_page(soup)
        for c in reviews:
            review_id = c.attrs['id'].split("_")[1]
            if options.verbose :
                print_info(review_id)
            overview = c.find(name="span", attrs={"class": "value-title"}).attrs['title']
            location = get_location(c)
            create_time = get_create_time(c)
            summary = get_summery(c)
            status, job = get_status_and_job(c)
            subrating = get_subrating_score(c)
            main_text = get_text(c, "mainText")
            pros = getProsCons(c, "Pros")
            cons = getProsCons(c, "Cons")
            advice_to_mgmt = get_text(c, "adviceMgmt")
            get_response(c, review_id)
            get_recommends(c.find(name="div", attrs={"class": "recommends"}), review_id)
            data = {
                "create_time": create_time,
                "review_id": review_id,
                "summary": summary,
                "overview": overview,
                "location": location,
                "status": status,
                "job": job,
                "main_text": main_text,
                "pros": pros,
                "cons": cons,
                "advice_to_management": advice_to_mgmt,
            }
            data.update(subrating)
            reviews_data[review_id] = data
        print_info("got " + str(total_reviews) + " reviews\r")
        if 0 < limit <= total_reviews:
            break
    return reviews_data


def get_next_page(soup):
    next_page = soup.find(name='span', text='Next').parent
    href = None
    if next_page is not None and next_page.name == 'a':
        href = base_url + next_page.attrs["href"]
    else:
        next_page = next_page.parent
        if next_page is not None and next_page.name == 'a':
            href = base_url + next_page.attrs["href"]
        else:
            return None
    if len(next_page.attrs['class']) > 1:
        return None
    return href


def get_create_time(c):
    if c.find("time") is not None:
        return c.find("time").attrs['datetime']
    return "N\A"


def get_summery(c):
    summery = c.find(name="span", attrs={"class": "summary"}).text
    if ord(summery[0]) == 34:
        return summery[1:-1]
    return summery


def get_status_and_job(c):
    status, job = ("", "")
    job_title_node = c.find(name="span", attrs={"class": "authorJobTitle"})
    if job_title_node is not None:
        job_title_txt = job_title_node.text
        try:
            status, job = job_title_txt.split(" - ")
        except ValueError:
            print(job_title_txt)
    return status, job


def get_text(c, class_name):
    text = ""
    node = c.find(name="p", attrs={"class": class_name})
    if node is not None:
        text = node.text
    return text


def getProsCons(c,section):
    text=""
    node = c.find("p", text=section).find_next_siblings("p")[0]
    if node is not None:
        text = node.text
    return text


def get_location(c):
    location = c.find(name="span", attrs={"class": "authorLocation"})
    if location is not None:
        location = location.text
    else:
        location = "N/A"
    return location


def get_subrating_score(c):
    subrating = {
        "Work/Life Balance": "0",
        "Culture & Values": "0",
        "Career Opportunities": "0",
        "Compensation and Benefits": "0",
        "Senior Management": "0"
    }
    sub_score = c.find(name="ul", attrs={"class": "undecorated"})
    if sub_score is not None:
        for item in sub_score.findAll(name="li"):
            class_name = item.find(name="div", attrs={"class": "minor"}).text
            class_score = item.find(name="span").attrs["title"]
            subrating[class_name] = class_score
    return subrating


def get_recommends(c, review_id=None):
    if c is not None:
        reviews = []
        for item in c.findAll(name="i", attrs={"class": "middle"}):
            reviews.append(item.parent.find(name="span").text)
        recommends[review_id]= {"review_id": review_id, "recommend": reviews}


def get_response(c, review_id=None):
    node = c.find(name="div", attrs={"class": "empRepComment"})
    response = {
        "review_id": review_id,
        "date": "",
        "role": "",
        "response": ""
    }
    if node is not None:
        header = node.find(name="div", attrs={"class": "commentHeader"})
        if header is not None:
            date, role = get_text(header,"minor").split(u' \u2013 ')
        response_text = get_text(node, "commentText")
        if len(response_text) > 0:
            response = {
                "review_id": review_id,
                "date": date,
                "role": role,
                "response": response_text
            }
    responses[review_id] = response


# def upload_to_db(df, df1, df2):
#     engine = create_engine(
#         'mysql+mysqldb://crawler_app:crawler_app@192.168.0.114:3306/crawler?charset=utf8&use_unicode=0', echo=False)
#     execute(df, "review", engine)
#     execute(df1, "hr_response", engine)
#     execute(df2, "employee_recommend", engine)
#     return 1
#
#
# def execute(df, table_name, engine):
#     engine.execute("truncate " + table_name + ";")
#     try:
#         df.to_sql(name=table_name, con=engine, if_exists='append', chunksize=1)
#     except:
#         print(sys.exc_info()[0])


def write_to_excel(df, df1, df2):
    writer = pd.ExcelWriter(output_filename, engine='xlsxwriter')
    df.to_excel(writer, sheet_name="db")
    df1.to_excel(writer, sheet_name="hr_response")
    df2.to_excel(writer, sheet_name="emp_recommend")
    writer.save()


def export_as_report(reviews):
    # gathering data
    report_data = []
    for r in reviews:
        cur_row = reviews[r]
        if r in responses:
            cur_row.update(responses[r])
        else:
            cur_row.update({"date":"","role":"","response":""})

        if r in recommends:
            cur_recommends = recommends[r]["recommend"]
            cur_row["recommends"] = ", ".join(cur_recommends)
        report_data.append(cur_row.values())
    df = DataFrame(report_data,columns=["create_time", "review_id", "summary",
                                        "overview", "location", "status", "job", "main_text",
                                        "pros", "cons", "advice_to_management",
                                        "Work/Life Balance", "Culture & Values", "Career Opportunities",
                                        "Compensation and Benefits", "Senior Management",
                                        "date", "role", "response", "recommends"])
    writer = pd.ExcelWriter(output_filename, engine='xlsxwriter')
    df.to_excel(writer, sheet_name="report")
    writer.save()


def export_as_db(reviews):
    df = DataFrame(reviews.values(),
                   columns=["create_time", "review_id", "summary", "overview", "location", "status", "job", "main_text",
                            "pros", "cons", "advice_to_management",
                            "Work/Life Balance","Culture & Values","Career Opportunities","Compensation and Benefits","Senior Management"])
    df1 = DataFrame(responses.values(), columns=["review_id", "date", "role", "response"])
    df2 = DataFrame(restruct_recommends_for_db_format(), columns=["review_id", "recommend"])
    df.set_index("review_id", inplace=True)
    df1.set_index("review_id", inplace=True)
    df2.set_index("review_id", inplace=True)
    write_to_excel(df, df1, df2)


def restruct_recommends_for_db_format():
    recommends_for_db_strct = []
    for r in recommends:
        for ir in recommends[r]["recommend"]:
            recommends_for_db_strct.append({"review_id": r, "recommend": ir})
    return recommends_for_db_strct


def main():
    reviews = get_reviews()
    if options.mode == 'report':
        export_as_report(reviews)
    else :
        export_as_db(reviews)


main()
